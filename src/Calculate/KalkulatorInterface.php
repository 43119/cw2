<?php

declare(strict_types=1);

namespace Src\Calculate;

interface KalkulatorInterface
{
    public function calculate(array $product): int;

}