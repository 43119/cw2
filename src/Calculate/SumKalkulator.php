<?php

declare(strict_types=1);

namespace Src\Calculate;

use Src\Model\Produkt;

class SumKalkulator implements KalkulatorInterface
{

    public function calculate(array $products): int
    {
        $sum = 0;

        foreach ($products as $product){
            /** @var Produkt $product */
            $sum += $product->getPrice();

        }
        return $sum;
    }
}