<?php

namespace Src\Calculate;

use Src\Model\Produkt;

class SumWithTaxKalkulator implements KalkulatorInterface
{
    private const TAX_RATE = 1.23;

    public function calculate(array $products): int
    {
        $sum = 0;

        foreach ($products as $product) {
            /** @var Produkt $product */
            $priceWithTax = (int) ($product->getPrice() * 100 * self::TAX_RATE / 100);
            $sum += $priceWithTax;
        }
        return $sum;
    }
}