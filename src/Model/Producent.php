<?php

declare(strict_types=1);

namespace Src\Model;

class Producent
{
    private string $name;
    private string $website;

    public function __construct(string $name, string $website)
    {
        $this->name = $name;
        $this->website = $website;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getWebsite(): string
    {
        return $this->website;
    }

}