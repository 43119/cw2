<?php

declare(strict_types=1);

namespace Src\Model;

class Marka
{
    private string $name;
    private Producent $producer;

    public function __construct(string $name, Producent $producer)
    {

        $this->name = $name;
        $this->producer = $producer;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getProducer(): Producent
    {
        return $this->producer;
    }

}