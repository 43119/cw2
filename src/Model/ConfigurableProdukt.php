<?php

declare(strict_types=1);

namespace Src\Model;

class ConfigurableProdukt extends  Produkt
{

    private string $color;
    private string $size;

    public function __construct(
        int    $id,
        string $name,
        Marka  $brand,
        int    $price,
        string $color,
        string $size
    )

    {
        parent::__construct($id, $name, $brand, $price);
        $this->color = $color;
        $this->size = $size;
    }

    public function isAvailable(): bool
    {
        // TODO: Implement isAvailable() method.
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function getSize(): string
    {
        return $this->size;
    }
}