<?php

declare(strict_types=1);

namespace Src\Model;

abstract class Produkt
{
    private int $id;
    private string $name;
    private Marka $brand;
    private int $price;

    public function __construct(int $id, string $name, Marka $brand, int $price)
    {

        $this->id = $id;
        $this->name = $name;
        $this->brand = $brand;
        $this->price = $price;
    }

    abstract public function isAvailable(): bool;

    public function getFullName(): string
    {
        return sprintf(
            '%s - %s (%s)',
            $this->name,
            $this->brand->getName(),
            $this->brand->getProducer()->getName()
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBrand(): Marka
    {
        return $this->brand;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}