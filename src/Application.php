<?php

namespace src;

use Src\Calculate\SumKalkulator;
use Src\Calculate\SumWithTaxKalkulator;
use Src\Factory\ObjectFactory;
use Src\Storage\StanMagazynu;

class Application
{
    public function run(): void
    {
        $sumCalculator = new SumKalkulator();
        $taxCalc = new SumWithTaxKalkulator();
        $factory = new ObjectFactory();
        $storage = new StanMagazynu();

        $products = $factory->create();

        foreach ($products as $product){
            $storage->addProduct($product, 10);

        }

        print sprintf('Suma ceny produktow wynosi: %d <br />', $sumCalculator->calculate(($products)));
        print sprintf('------------------------------------------------------<br />');
        print sprintf('Suma ceny produktow plus podatek wynosi: %d <br />', $taxCalc->calculate(($products)));
        print sprintf('------------------------------------------------------<br />');
        print sprintf('Laczna ilosc produktow: %d <br />', $storage->getTotalStockAmount());
        print sprintf('------------------------------------------------------<br />');


    }
}
