<?php

declare(strict_types=1);

namespace Src\Factory;

use Src\Model\Marka;
use Src\Model\ConfigurableProdukt;
use Src\Model\Producent;
use Src\Model\SimpleProdukt;
use Src\Model\VirtualProdukt;

class ObjectFactory
{
    public function create(): array
    {
        $producer = new Producent('Jakis producent', 'przykladowo.com');
        $brand = new Marka('Jakas marka', $producer);

        $product1 = new SimpleProdukt(1, 'Koszulka', $brand, 2000);
        $product2 = new VirtualProdukt(2, 'Newsletter sub', $brand, 500, true);
        $product3 = new ConfigurableProdukt(3, 'Sukienka', $brand, 7000, 'Czerwony', 'S');

        return [
          $product1,
            $product2,
            $product3
        ];
    }

}